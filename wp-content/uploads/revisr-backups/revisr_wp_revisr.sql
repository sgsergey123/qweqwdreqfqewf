
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_revisr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_revisr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text,
  `event` varchar(42) NOT NULL,
  `user` varchar(60) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_revisr` WRITE;
/*!40000 ALTER TABLE `wp_revisr` DISABLE KEYS */;
INSERT INTO `wp_revisr` VALUES (1,'2017-08-09 07:40:25','Created new branch: dev','branch','admin'),(2,'2017-08-09 07:40:30','Checked out branch: dev.','branch','admin'),(3,'2017-08-09 07:41:51','Successfully backed up the database.','backup','admin'),(4,'2017-08-09 07:41:51','There was an error committing the changes to the local repository.','error','admin'),(5,'2017-08-09 07:46:00','Merged branch master into branch dev.','merge','admin'),(6,'2017-08-09 07:46:56','Successfully backed up the database.','backup','admin'),(7,'2017-08-09 07:46:57','There was an error committing the changes to the local repository.','error','admin'),(8,'2017-08-09 07:48:02','Successfully backed up the database.','backup','Revisr Bot'),(9,'2017-08-09 07:48:02','The daily backup was successful.','backup','Revisr Bot'),(10,'2017-08-09 07:48:18','There was an error committing the changes to the local repository.','error','admin'),(11,'2017-08-09 07:48:36','Committed <a href=\"http://wordpress:81/wp-admin/admin.php?page=revisr_view_commit&commit=3958d6f&success=true\">#3958d6f</a> to the local repository.','commit','admin'),(12,'2017-08-09 07:49:44','Successfully backed up the database.','backup','admin'),(13,'2017-08-09 07:49:44','Committed <a href=\"http://wordpress:81/wp-admin/admin.php?page=revisr_view_commit&commit=30789e2&success=true\">#30789e2</a> to the local repository.','commit','admin'),(14,'2017-08-09 07:55:00','Successfully created a new repository.','init','admin'),(15,'2017-08-09 07:55:24','Error pushing changes to the remote repository.','error','admin'),(16,'2017-08-09 07:55:51','There was an error committing the changes to the local repository.','error','admin'),(17,'2017-08-09 07:57:23','Successfully backed up the database.','backup','admin'),(18,'2017-08-09 07:57:23','Committed <a href=\"http://wordpress:81/wp-admin/admin.php?page=revisr_view_commit&commit=64cd982&success=true\">#64cd982</a> to the local repository.','commit','admin'),(19,'2017-08-09 08:03:11','Committed <a href=\"http://wordpress:81/wp-admin/admin.php?page=revisr_view_commit&commit=1f2eb39&success=true\">#1f2eb39</a> to the local repository.','commit','admin'),(20,'2017-08-09 08:06:31','Successfully pushed 3 commits to qweqwdreqfqewf/master.','push','admin'),(21,'2017-08-09 08:07:36','Committed <a href=\"http://wordpress:81/wp-admin/admin.php?page=revisr_view_commit&commit=fa84446&success=true\">#fa84446</a> to the local repository.','commit','admin'),(22,'2017-08-09 08:07:38','Successfully pushed 1 commit to qweqwdreqfqewf/master.','push','admin'),(23,'2017-08-09 09:46:29','There was an error committing the changes to the local repository.','error','admin'),(24,'2017-08-09 09:46:49','Successfully pushed 5 commits to qweqwdreqfqewf/master.','push','admin'),(25,'2017-08-09 09:47:59','There was an error committing the changes to the local repository.','error','admin'),(26,'2017-08-09 09:48:36','There was an error committing the changes to the local repository.','error','admin'),(27,'2017-08-09 09:48:41','Committed <a href=\"http://wordpress:81/wp-admin/admin.php?page=revisr_view_commit&commit=2de80eb&success=true\">#2de80eb</a> to the local repository.','commit','admin'),(28,'2017-08-09 09:48:44','Successfully pushed 1 commit to qweqwdreqfqewf/master.','push','admin'),(29,'2017-08-09 09:55:29','Created new branch: dev','branch','admin'),(30,'2017-08-09 09:55:29','Checked out branch: dev.','branch','admin'),(31,'2017-08-09 10:10:11','Successfully pushed 3 commits to qweqwdreqfqewf/dev.','push','admin'),(32,'2017-08-09 10:38:33','Successfully backed up the database.','backup','admin'),(33,'2017-08-09 10:38:36','Successfully pushed 2 commits to qweqwdreqfqewf/dev.','push','admin'),(34,'2017-08-09 10:41:14','Successfully pushed 2 commits to qweqwdreqfqewf/dev.','push','admin'),(35,'2017-08-09 10:41:27','Successfully backed up the database.','backup','Revisr Bot'),(36,'2017-08-09 10:41:30','Successfully pushed 1 commit to qweqwdreqfqewf/dev.','push','Revisr Bot'),(37,'2017-08-09 10:41:30','The daily backup was successful.','backup','Revisr Bot'),(38,'2017-08-09 10:42:23','Successfully pushed 1 commit to qweqwdreqfqewf/dev.','push','admin'),(39,'2017-08-09 10:42:32','Successfully pushed 0 commits to qweqwdreqfqewf/dev.','push','admin'),(40,'2017-08-09 10:43:02','Successfully imported the database. ','import','admin'),(41,'2017-08-09 10:43:21','Successfully pushed 2 commits to qweqwdreqfqewf/master.','push','admin'),(42,'2017-08-09 13:12:29','Successfully backed up the database.','backup','admin'),(43,'2017-08-09 13:12:32','Successfully pushed 1 commit to qweqwdreqfqewf/master.','push','admin'),(44,'2017-09-14 11:50:16','Successfully backed up the database.','backup','Revisr Bot'),(45,'2017-09-14 11:50:24','Successfully pushed 2 commits to qweqwdreqfqewf/master.','push','Revisr Bot'),(46,'2017-09-14 11:50:24','The daily backup was successful.','backup','Revisr Bot'),(47,'2017-09-14 12:54:15','Successfully pushed 2 commits to qweqwdreqfqewf/master.','push','admin'),(48,'2017-09-14 12:54:22','Successfully backed up the database.','backup','admin'),(49,'2017-09-14 12:54:26','Successfully pushed 1 commit to qweqwdreqfqewf/master.','push','admin');
/*!40000 ALTER TABLE `wp_revisr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

