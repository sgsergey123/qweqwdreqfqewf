
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_vp_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_vp_id` (
  `vp_id` binary(16) NOT NULL,
  `table` varchar(64) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`vp_id`),
  UNIQUE KEY `table_id` (`table`,`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_vp_id` WRITE;
/*!40000 ALTER TABLE `wp_vp_id` DISABLE KEYS */;
INSERT INTO `wp_vp_id` VALUES ('+�B)CL��\��\�\�','comments',1),('\��.\�YF��2c|\�	�','postmeta',13),('|��x�=Hʱ\�kҀA)M','postmeta',107),('�%�\\|�IV��쩰\�\�','postmeta',108),('^h(�gGa���\�]�\�\�','postmeta',109),('\���_��I����\�]�9','postmeta',110),('��Y�\�F��\�h\�=���','postmeta',111),('\�Un���N\�\�W^\ZX2','postmeta',112),('Āb��\�E��Y�C�\�4','postmeta',113),('\�\0�\�y?G�\�C����','postmeta',114),('��hKFKu�]��N','postmeta',115),('\�!<\��Ca��m�l�\n3','postmeta',116),('��*�\�OŮ>A�\��\�','postmeta',117),('\0���Bi�f2���\\\0','postmeta',118),('�\\xh65MK�C�\'���','postmeta',119),('\r�\�!RE����$kxK�','postmeta',120),('�iE\�\�O��\�X֤1\�-','postmeta',121),('l\�$z\�@AH�a��K\�p>','postmeta',122),('�\�\�r\0dF*��V\�w];\�','postmeta',123),('��$]A��?]�,T','postmeta',124),('\��F�\r%Md��\�\��Dg','postmeta',125),('�2P�F颭KJ�gt�','postmeta',126),('��\\S�E���+�zQ�?','postmeta',127),('\�!\�nHj�a\�?v��\�','postmeta',128),('tI�\�qJ�Y|Y2���','postmeta',129),('Zc\"CU9D��\�y<�A','postmeta',130),('���\���D �\�\�Z-�\�\�','postmeta',131),('\�\�DN\�DQ�����','postmeta',132),('͜�\��F��\�\�^c�{','postmeta',133),('\�%{�I��eiL��O','postmeta',134),('LYv\Z9Bp�wS͵�s\�','postmeta',135),('5w�_��Fj��g$%J\\','postmeta',136),('z\�j\�]O~�\�f��9z','postmeta',137),('6>k�%M��Ɔ���G�','postmeta',140),('\\\�\�|G�`��p���','postmeta',143),('-\ZR<\�K����E<\�!','postmeta',146),('�m�\�\�G��劾=\�9\r','postmeta',149),('\�\��kDܯS�\\\\\��','postmeta',152),('?\��sJ�LιH%��t�','postmeta',158),('�\���LOШȘ\�CG&','postmeta',159),('�Dx6b+GJ�eX\��30','postmeta',160),('��+�XIѝ=��K.~','postmeta',161),('\�1�z\�\�Aأfv���xQ','postmeta',162),('z��W��F���Pҥ��','postmeta',163),('��55�B@�����:\�d','postmeta',164),('\�4F3O �d[Y\�\�','postmeta',165),('\�\��~\�F&��\�P\���','postmeta',166),('�\�u�\�Id�b!Q<C�','postmeta',167),('1}[*ڨ@��s�Q%�\�','postmeta',168),('(Yg\�\�DP�\">��\��','postmeta',169),('�Gwj�IN���9/\��','postmeta',170),('譐��lDv��2J�P\�','postmeta',171),('\�|\�6�Mя:\�\�]�\�','postmeta',172),('N��k�.E��iJ\�6`W\�','postmeta',173),('+��\�\�F��5\�\���ى','postmeta',174),(',|ݺ�`MJ�>Ȉ\�uW)','postmeta',175),('�\�\�^�kC��/\�j\�','postmeta',176),('1�expTI/�\\ˇx�_','postmeta',177),('8S\�\�yJƖK\�\���','postmeta',178),('���ĔNТ�^485\�','postmeta',179),('f5\�CfFt�CW�\"RK','postmeta',180),('�\�Ge��ٚ�=��','postmeta',181),('a�,\��OΘӒ�\�nr�','postmeta',182),('c3�\�\�NY��h\Z\\\�\"\�','postmeta',183),('P̓<�vDo�d\�5�<\0','postmeta',184),('�\�?%\�6Dɵ�G,\�Y�','postmeta',185),('\��J4�v�\�:�+2','postmeta',186),('u:�NG\Z��p�}q�\�','postmeta',187),('��1\\E\0���P�Խ','postmeta',188),('1\�?�N˲Y�\"{','posts',1),('��\ZF�	Ni��O�x�O','posts',7),('�d\\\0Z-N,��n/��','posts',34),('xٰ�\�~N��¢|\�,�','posts',35),('](\��D��5\"d\�ґ&','posts',36),('\��.�N��\�!o�<\�$','posts',37),('(�\�g\�\�J��\0!��b\�','posts',38),('Uk��ļOǏ͗�&%�','posts',40),('d�94$K��8�\�RҘ','posts',42),('�\�k:�^@��(����o}','posts',44),('���J��K�\�t\Z\0�','posts',46),('i�\�\�ʮJ\r����k\�','posts',50),(']Ne�\�\�I҈\�`+�\�\�','posts',52),('�,:�\�\�G��\�\0�\�\\�(','posts',53),('��cp�:H���|&�\�1','posts',57),('zиΆrM;�z\Z𪸒','terms',1),('\�����^H������','term_taxonomy',1),('��g|!FX�+\�[\�N�','usermeta',1),('!�\�Z\�M�����Z�aI','usermeta',2),('\�c���EݬU꡺�G(','usermeta',3),('�4ma�\�L��u[g\�G�','usermeta',4),('A\�fHZ0@��^\��\�','usermeta',5),('/V`a�&B��W\����','usermeta',6),('��\�NwCAy���\�vT\�\�','usermeta',7),('V�Z�\�M^�p��\�\�\�\�','usermeta',8),('?\��_E��\�8�<:\Z','usermeta',9),('���<�Gйe�Xy�\�^','usermeta',10),('�u��`�HK�\��r\�;','usermeta',11),('4�\'��\�C_�vH\��\�G','usermeta',12),('�FK�\�F��X+k�','usermeta',13),('7\�.HX\rJ��:#(�n�','usermeta',14),('�(\�BqD��2q�ZV\�<','usermeta',17),('\�:�j�\�M$�vH��ݧ\�','usermeta',20),('A�҆F\�L��J\n\�\���Y','usermeta',21),('^�\�\�q[Oy��ϲ�\��','usermeta',23),('\'=���G\�3�f]m��','usermeta',24),('^�\��q\�F��\�X#���','usermeta',25),('�q��\�Ce��\�\�h\�`�','users',1);
/*!40000 ALTER TABLE `wp_vp_id` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

